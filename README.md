# Programming Language: Julia

This project is **Julia** implementation of the following problem:
* Finite Difference Method (FDM) applied to distribution of the electrostatic voltage inside a source-less rectangular region [[View Repository on GitLab]](https://gitlab.com/oameed/cem_fdm_2d)

## References

_Useful Web Tutorials_

[[1].](https://github.com/JuliaDebug/Debugger.jl) 0000. _Julia's Debugger_  
[[2].](http://webpages.csus.edu/fitzgerald/julia-debugger-tutorial/) 2020. fitzgerald. _Introduction to Julia’s Debugger_  
[[3].](https://juliaio.github.io/HDF5.jl/stable/) 0000. _HDF5 for Julia_

## Code Statistics
<pre>
github.com/AlDanial/cloc v 1.74  T=0.01 s (601.4 files/s, 31003.6 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Julia                            5             53             25            236
Markdown                         1             15              0             49
YAML                             1              1              0             39
MATLAB                           1              6              5             17
Bourne Shell                     1              7              0             11
-------------------------------------------------------------------------------
SUM:                             9             82             30            352
-------------------------------------------------------------------------------
</pre>

## How to Run

* This project uses [**Julia**](https://julialang.org/) Version 1.1. The `YAML` file for creating the conda environment used to run this project is included in `run/conda`
* After installation and before first use, run Julia and do the following:
  1. `import Pkg`
  2. `Pkg.add("Debugger")`
  3. `Pkg.add("ArgParse")`
  4. `Pkg.add("HDF5")`
  5. `Pkg.add("Plots")`
  6. `Pkg.add("PyPlot")`
* To Debug using _BreakPoints_:
  1. place `using Debugger` and `@bp` inside the function of interest e.g. foo()
  2. enter REPL mode in the same directory that `main-filename.jl` exists. 
  3. `using Debugger`
  4. `include("main-filename.jl")`
  5. `@run foo()` 
  * When in debug mode, use the backtick key `` ` `` to enter into calculation mode and `Ctrl+c` to exit 
  * use `q` to quit debug mode
  * _note_: command line arguments of type _'required'_ don't work with debugger
* To run simulations:
  1. `cd` to the main project directory
  2. `./run/sh/run_v01.sh`

## Simulation 'v01':

|     |
|:---:|
![][fig1]

[fig1]:simulations/v01/data.png






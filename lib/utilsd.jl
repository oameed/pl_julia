######################################
### FINITE DIFFERENCE METHOD (FDM) ###
### UTILITY FUNCTION DEFINITIONS   ###
### by: OAMEED NOAKOASTEEN         ###
######################################

module utilsd

export wHDF

using HDF5

function wHDF(FILENAME,DIRNAMES,DATA)
 fobj  =h5open(FILENAME,"w")
 for i in 1:length(DIRNAMES)
  fobj[DIRNAMES[i][1]]=DATA[i][1]
 end
 close(fobj)
end

end



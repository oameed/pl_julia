#############################################
### FINITE DIFFERENCE METHOD (FDM)        ###
### GEOMETRIC DOMAIN FUNCTION DEFINITIONS ###
### by: OAMEED NOAKOASTEEN                ###
#############################################

module geomd 

export get_nodes_bnd

function get_1d_boundary_index(J,I,X,Y,NODES,LIST)
 BND=Vector{Float64}[]
 for  j in J
  for i in I
   push!(BND,[X[i],Y[j]])
  end
 end
 push!(LIST,[findall(x->x==i,NODES)[1] for i in BND])
end

function get_0d_boundary_index(COORD,NODES,LIST)
 push!(LIST,[findall(x->x==i,NODES)[1] for i in COORD])
end

function get_nodes_bnd(XEND,YEND,N)
 
 DIGITS     =5
 delta      =(XEND-0)/N
 Ny         =Int(floor((YEND-0)/delta))
 x          =[round(i*delta; digits=DIGITS) for i in 0:N ]
 y          =[round(i*delta; digits=DIGITS) for i in 0:Ny]
 
 nodes      =Vector{Float64        }[]                                                        #   LIST OF ALL NODES OF THE GRID
 bound      =Vector{Int            }[]                                                        #   LIST OF ALL NODES OF THE GRID THAT ARE ON THE BOUNDARIES
 neighbors  =Vector{Int            }[]                                                        #   LIST OF INTERIOR NEIGHBORS       FOR BOUNDARY NODES
 NEIGHBORS  =Vector{Int            }[]                                                        #   LIST OF          NEIGHBORS       FOR INTERIOR NODES
 ngbr       =Vector{Int            }[]                                                        #   LIST OF          NEIGHBORS       FOR ALL GRID POINTS
 bndngbr    =Vector{Vector{Float64}}[]                                                        #   LIST OF EXTERIOR NEIGHBOR COORDS FOR BOUNDARY NODES
 
 for  j in 1:length(y)                                                                        ### GET ALL NODES
  for i in 1:length(x)
   push!(nodes,[x[i],y[j]])
  end
 end
 
 get_1d_boundary_index([1]          ,2:length(x)-1,x,y,nodes,bound)                           ### GET NODES ON 1D BOUNDARY
 get_1d_boundary_index(2:length(y)-1,  length(x)  ,x,y,nodes,bound)
 get_1d_boundary_index(  length(y)  ,2:length(x)-1,x,y,nodes,bound)
 get_1d_boundary_index(2:length(y)-1,[1]          ,x,y,nodes,bound)
 get_0d_boundary_index([[0.0   ,0.0   ]]              ,nodes,bound)                           ### GET NODES ON 0D BOUNDARY
 get_0d_boundary_index([[x[end],0.0   ]]              ,nodes,bound)
 get_0d_boundary_index([[x[end],y[end]]]              ,nodes,bound)
 get_0d_boundary_index([[0.0   ,y[end]]]              ,nodes,bound)
 
 for i in 1:length(bound[1])                                                                  ### GET INTERIOR NEIGHBORS FOR 1D BOUNDARY NODES
  node              =nodes[bound[1][i]]
  node_to_the_right =[round(node[1]+delta;digits=DIGITS),      node[2]                     ]
  node_to_the_top   =[      node[1]                     ,round(node[2]+delta;digits=DIGITS)]
  node_to_the_left  =[round(node[1]-delta;digits=DIGITS),      node[2]                     ]
  push!(neighbors,[findall(x->x==node_to_the_right,nodes)[1],
                   findall(x->x==node_to_the_top  ,nodes)[1],
                   findall(x->x==node_to_the_left ,nodes)[1] ])
  node_to_the_bottom=[      node[1]                     ,round(node[2]-delta;digits=DIGITS)]  ### GET EXTERIOR NEIGHBOR COORDS FOR 1D BOUNDARY NODES
  push!(bndngbr  ,[              node_to_the_bottom          ])
 end

 for i in 1:length(bound[2])
  node              =nodes[bound[2][i]]
  node_to_the_top   =[      node[1]                     ,round(node[2]+delta;digits=DIGITS)]
  node_to_the_left  =[round(node[1]-delta;digits=DIGITS),      node[2]                     ]
  node_to_the_bottom=[      node[1]                     ,round(node[2]-delta;digits=DIGITS)]
  push!(neighbors,[findall(x->x==node_to_the_top   ,nodes)[1],
                   findall(x->x==node_to_the_left  ,nodes)[1],
                   findall(x->x==node_to_the_bottom,nodes)[1] ])
  node_to_the_right =[round(node[1]+delta;digits=DIGITS),      node[2]                     ]
  push!(bndngbr  ,[              node_to_the_right            ])
 end

 for i in 1:length(bound[3])
  node              =nodes[bound[3][i]]
  node_to_the_bottom=[      node[1]                     ,round(node[2]-delta;digits=DIGITS)]
  node_to_the_right =[round(node[1]+delta;digits=DIGITS),      node[2]                     ]
  node_to_the_left  =[round(node[1]-delta;digits=DIGITS),      node[2]                     ]
  push!(neighbors,[findall(x->x==node_to_the_bottom,nodes)[1],
                   findall(x->x==node_to_the_right ,nodes)[1],
                   findall(x->x==node_to_the_left  ,nodes)[1] ])
  node_to_the_top   =[      node[1]                     ,round(node[2]+delta;digits=DIGITS)]
  push!(bndngbr  ,[              node_to_the_top              ])
 end

 for i in 1:length(bound[4])
  node              =nodes[bound[4][i]]
  node_to_the_bottom=[      node[1]                     ,round(node[2]-delta;digits=DIGITS)]
  node_to_the_right =[round(node[1]+delta;digits=DIGITS),      node[2]                     ]
  node_to_the_top   =[      node[1]                     ,round(node[2]+delta;digits=DIGITS)]
  push!(neighbors,[findall(x->x==node_to_the_bottom,nodes)[1],
                   findall(x->x==node_to_the_right ,nodes)[1],
                   findall(x->x==node_to_the_top   ,nodes)[1] ])
  node_to_the_left  =[round(node[1]-delta;digits=DIGITS),      node[2]                     ]
  push!(bndngbr  ,[              node_to_the_left             ])
 end

 node              =nodes[bound[5][1]]                                                        ### GET INTERIOR NEIGHBORS FOR 0D BOUNDARY NODES
 node_to_the_right =[round(node[1]+delta;digits=DIGITS),      node[2]                     ]
 node_to_the_top   =[      node[1]                     ,round(node[2]+delta;digits=DIGITS)]
 push!(neighbors,[findall(x->x==node_to_the_right ,nodes)[1],
                  findall(x->x==node_to_the_top   ,nodes)[1] ])
 node_to_the_left  =[round(node[1]-delta;digits=DIGITS),      node[2]                     ]   ### GET EXTERIOR NEIGHBOR COORDS FOR 0D BOUNDARY NODES
 node_to_the_bottom=[      node[1]                     ,round(node[2]-delta;digits=DIGITS)]
 push!(bndngbr  ,[              node_to_the_left  ,
                                node_to_the_bottom           ])
 
 node              =nodes[bound[6][1]]
 node_to_the_top   =[      node[1]                     ,round(node[2]+delta;digits=DIGITS)]
 node_to_the_left  =[round(node[1]-delta;digits=DIGITS),      node[2]                     ]
 push!(neighbors,[findall(x->x==node_to_the_top   ,nodes)[1],
                  findall(x->x==node_to_the_left  ,nodes)[1] ])
 node_to_the_right =[round(node[1]+delta;digits=DIGITS),      node[2]                     ]
 node_to_the_bottom=[      node[1]                     ,round(node[2]-delta;digits=DIGITS)]
 push!(bndngbr  ,[              node_to_the_right ,
                                node_to_the_bottom           ])
 
 node              =nodes[bound[7][1]]
 node_to_the_bottom=[      node[1]                     ,round(node[2]-delta;digits=DIGITS)]
 node_to_the_left  =[round(node[1]-delta;digits=DIGITS),      node[2]                     ]
 push!(neighbors,[findall(x->x==node_to_the_bottom,nodes)[1],
                  findall(x->x==node_to_the_left  ,nodes)[1] ])
 node_to_the_right =[round(node[1]+delta;digits=DIGITS),      node[2]                     ]
 node_to_the_top   =[      node[1]                     ,round(node[2]+delta;digits=DIGITS)]
 push!(bndngbr  ,[              node_to_the_right ,
                                node_to_the_top              ])
 
 node              =nodes[bound[8][1]]
 node_to_the_bottom=[      node[1]                     ,round(node[2]-delta;digits=DIGITS)]
 node_to_the_right =[round(node[1]+delta;digits=DIGITS),      node[2]                     ]
 push!(neighbors,[findall(x->x==node_to_the_bottom,nodes)[1],
                  findall(x->x==node_to_the_right ,nodes)[1] ])
 node_to_the_top   =[      node[1]                     ,round(node[2]+delta;digits=DIGITS)]
 node_to_the_left  =[round(node[1]-delta;digits=DIGITS),      node[2]                     ]
 push!(bndngbr  ,[              node_to_the_top   ,
                                node_to_the_left             ])
 
 bound      =vcat(bound[1],bound[2],bound[3],bound[4],bound[5],bound[6],bound[7],bound[8])    ### SORT BOUNDARY POINTS ACCORDING TO THEIR NUMBER
 indexsorted=sortperm(bound)
 bound      =[bound[i]     for i in indexsorted]
 neighbors  =[neighbors[i] for i in indexsorted]                                              ### SORT INTERIOR NEIGHBOR INFORMATION ACCORDINGLY
 bndngbr    =[bndngbr[i]   for i in indexsorted]                                              ### SORT EXTERIOR NEIGHBOR INFORMATION ACCORDINGLY
 
 indexinside=[i for i in 1:length(nodes) if !(i in bound)]                                    ### GET  NEIGHBORS FOR INTERIOR NODES
 for i in 1:length(indexinside)
  node              =nodes[indexinside[i]]
  node_to_the_bottom=[      node[1]                     ,round(node[2]-delta;digits=DIGITS)]
  node_to_the_right =[round(node[1]+delta;digits=DIGITS),      node[2]                     ]
  node_to_the_top   =[      node[1]                     ,round(node[2]+delta;digits=DIGITS)]
  node_to_the_left  =[round(node[1]-delta;digits=DIGITS),      node[2]                     ]
  push!(NEIGHBORS,[findall(x->x==node_to_the_bottom,nodes)[1],
                   findall(x->x==node_to_the_right ,nodes)[1],
                   findall(x->x==node_to_the_top   ,nodes)[1],
                   findall(x->x==node_to_the_left  ,nodes)[1] ])
 end
 
 for i in 1:length(nodes)                                                                     ### GENERATE AN ORDERED LIST OF NEIGHBORS FOR ALL GRID POINTS
  if i in bound
   INDEX =findall(x->x==i,bound      )[1]
   LIST  =neighbors
  else
   if i in indexinside
    INDEX=findall(x->x==i,indexinside)[1]
    LIST =NEIGHBORS
   end
  end
  push!(ngbr,LIST[INDEX])
 end
 
 return nodes, ngbr, bound, bndngbr, delta, Ny
end

end



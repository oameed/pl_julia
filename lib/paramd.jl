########################################
### FINITE DIFFERENCE METHOD (FDM)   ###
### PARAMETER DEFINITIONS            ###
### by: OAMEED NOAKOASTEEN           ###
########################################

module paramd

export parse_commandline

using ArgParse

function parse_commandline()
 parser    =ArgParseSettings()              # DEFINE PARSER
 @add_arg_table! parser begin               # DEFINE PARAMETERS
  "-v"
   help    = "SPECIFY SIMULATION VERSION"
  "--lx"
   arg_type= Float64
   default = 1.0
   help    = "LENGTH  OF DOMAIN IN X DIRECTION"
  "--ly"
   arg_type= Float64
   default = 0.5
   help    = "LENGTH  OF DOMAIN IN Y DIRECTION"
  "-n"
   arg_type= Int
   default = 100
   help    = "NUMBER  OF DIVISIONS ALONG X-AXIS"
  "--volts"
   arg_type= Float64
   default = 1.0
   help    = "BOUNDARY VOLTAGE AMPLITUDE"
 end
 return parse_args(parser)                   
end

end



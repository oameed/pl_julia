#########################################
### FINITE DIFFERENCE METHOD (FDM)    ###
### MATRIX A AND VECTOR G DEFINITIONS ###
### by: OAMEED NOAKOASTEEN            ###
#########################################

module mAvGd

export get_matrix_A, get_vector_G

function get_matrix_A(NGBR)
 A=zeros(length(NGBR),length(NGBR))
 for  i in 1:length(NGBR)
  A[i,i]          =-4
  for j in 1:length(NGBR[i])
   A[i,NGBR[i][j]]=1
  end
 end
 return A
end

function get_vector_G(SIZE,BND,BNDEXT,DX,V)
 G=zeros(SIZE)
 for i in 1:length(BND)
  flag=[p for p in BNDEXT[i] if p[1]<0]
  if !isempty(flag)
   for point in BNDEXT[i]
    G[BND[i]]=G[BND[i]]+1*V
   end
  end
 end
 return G
end

end



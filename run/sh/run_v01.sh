#! /bin/bash

source  activate jlenv

cd run/fdm

echo ' CREATING PROJECT DIRECTORY '                            'v01'
rm     -rf                                    ../../simulations/v01
tar    -xzf  ../../simulations/v00.tar.gz -C  ../../simulations
mv           ../../simulations/v00            ../../simulations/v01

echo ' RUNNING FINITE DIFFERENCE SIMULATION'
julia solver.jl -v v01 -n 50

echo ' RUNNING VISUALIZATIONS '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v01','v');exit;"



######################################
### FINITE DIFFERENCE METHOD (FDM) ###
### SOLVER                         ###
### by: OAMEED NOAKOASTEEN         ###
######################################

push!(LOAD_PATH,joinpath(pwd(),"..","..","lib"))
import paramd,
       utilsd, 
       geomd ,
       mAvGd 

args                       =paramd.parse_commandline()                         # ENABLE FLAGS

nodes,ngbr,bnd,bndext,dx,Ny=geomd.get_nodes_bnd(args["lx"],
                                                args["ly"],
                                                args["n" ] )
A                          =mAvGd.get_matrix_A( ngbr       )
G                          =mAvGd.get_vector_G(size(A,1)    ,
                                               bnd          ,
                                               bndext       ,
                                               dx           ,
                                               args["volts"] )
v                          =A\(-G)
v                          =reshape(v,(args["n"]+1,Ny+1))

utilsd.wHDF(joinpath(pwd(),"..","..","simulations",args["v"],"data"*".h5"),
            Vector{String          }[["A"],["v"]]                         ,
            Vector{Array{Float64,2}}[[ A ],[ v ]]                          )

println("FINISHED!")



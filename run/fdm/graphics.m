%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FINITE DIFFERENCE METHOD (FDM) %%%
%%% GRAPHICS                       %%%
%%% by: OAMEED NOAKOASTEEN         %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function graphics(V,DIR)
close all
clc

PATH=fullfile('..','..','simulations',V)                          ;

v   =h5read(fullfile(PATH,strcat('data','.h5')),fullfile('/',DIR));

surf(v)
title('Numerical')
colormap default
shading  interp
axis     equal
view(2)
set(gca,'xticklabel',[]   ,...
        'yticklabel',[]   ,...
        'xtick'     ,[]   ,...
        'ytick'     ,[]       )

saveas(gca,fullfile(PATH,strcat('data','.png')),'png')

end
